import csv
import itertools
import math
import numpy as np
import matplotlib.pyplot as plt
import plotly.plotly as py
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from plotly.tools import FigureFactory as FF
from scipy import stats


def estimate_beta1(x, y):  # Function that estimates Beta1
    x_y_covariance = np.cov(x, y)  # by devising X and Y's Covariance by X's variance which is equal to equation (14.5)
    x_variance = np.var(x)
    return x_y_covariance[0][1] / x_variance


def calculate_log_likelihood(y, y_hat, sigma):
    n = y.size
    return -n * math.log10(sigma) - ((1 / 2 * math.pow(sigma, 2)) * calculate_rss(y, y_hat))


def calculate_aic(parameter_num, log_likelihood):
    return log_likelihood - parameter_num


def calculate_rss(y, y_hat):
    rss = 0  # estimates sigma square by using the estimates of Y
    for y_hat, yi in itertools.izip(y_hat, y):
        epsilon = yi - y_hat
        rss += math.pow(epsilon, 2)
    return rss


def estimate_beta0(x, y, beta1hat):
    return np.mean(y) - beta1hat * np.mean(x)


def estimate_sigma2(x, y, beta0hat, beta1hat):  # Function that takes in all the X's and the Y's and
    rss = 0  # estimates sigma square by using the estimates of Y
    for xi, yi in itertools.izip(x, y):
        epsilon = yi - (beta0hat + beta1hat * xi)
        rss += math.pow(epsilon, 2)
    return rss / (x.size - 2)


def estimate_sigma2pr(y, y_hat, n, k):  # Function that takes in all the X's and the Y's and
    rss_ = 0  # estimates sigma square by using the estimates of Y
    for yi, y_hati in itertools.izip(y,
                                     y_hat):  # Another method like the one above but tuned to be convient for forwarding method.
        epsilon = yi - y_hati
        rss_ += math.pow(epsilon, 2)
    return rss_ / (n - k)


def calculate_r2(rss, y):
    y_bar = np.mean(y)
    tss = 0
    for yi in y:
        tss += math.pow(yi - y_bar, 2)
    return 1 - (rss / tss)


def calculate_se_beta0(sigma_hat, x):
    sx2 = np.var(x)
    x2 = pow(x, 2)
    return (sigma_hat / (math.sqrt(sx2) * math.sqrt(x.size))) * (math.sqrt(sum(x2) / x.size))


def calculate_se_beta1(sigma_hat, x):
    sx2 = np.var(x)
    return sigma_hat / (math.sqrt(sx2) * math.sqrt(x.size))


# PREPARATION START

with open('Dataset1.csv', 'rb') as csv_file:
    spam_reader = csv.reader(csv_file, delimiter=' ')
    data_ = []  # Array which we load the data file into
    for row in spam_reader:
        data_.append(str(row[0]).split(','))

data_ = [[float(string) for string in inner] for inner in data_]  # Convert the strings to int's for easy usage.
data = np.array(data_)  # convert the data array to a numpy array to be able to use it's features.

# print(data_)
# data[x][y] x determines which data we want, y determines which feature we want.
# the 9th element in each row is the target variable.
# e.g: data[1][2] is the third FEATURE in the second row of DATA.
# e.g data[:, 1] is the array of all the second FEATURES (second column of all the data)
training_data = data[:400]  # The first 400 elements of data are used for training.
testing_data = data[400:]  # The remaining 100 elements are used for testing.

feature = []  # The i'th feature is feature[i]
for i in range(0, 8):
    feature.append(training_data[:, i])
Y = training_data[:, 8]  # List of the target variables.
Yn_bar = np.mean(Y)  # Mean of target variables.

feature_test = []  # The i'th feature is feature[i]
for i in range(0, 8):
    feature_test.append(testing_data[:, i])
Y_test = testing_data[:, 8]  # List of the target variables.

X_test_matrix_array = np.column_stack([1 for j in range(0, feature_test[0].size)])
X_test_matrix_array.shape = (feature_test[0].size, 1)  # make a column of 1's for calculating beta0
for i in range(0, 8):
    X_test_matrix_array = np.column_stack((X_test_matrix_array,
                                       feature_test[i]))

# PREPARATION END


# PART a START

# for i in range(0, 8):  # Scatter plot start.
#     plt.subplot(4, 2, i + 1)
#     plt.title('Variable ' + str(i+1))
#     plt.xlabel('Variable ' + str(i+1))
#     plt.ylabel('Target Variable')
#     plt.scatter(x=data[:, i], y=data[:, 8])

# Uncomment this line to plot the figures.
# plt.show()  # Scatter plot end.

# PART a END

# PART B START

beta0_hats = []  # The Beta0's of the data. the i'th element is the estimated Beta0 for the i'th variable.
beta0_se = []  # The standard error of Beta0's. the i'th element is the standard error of Beta0 for the i'th variable.
beta1_hats = []  # The Beta1's of the data. the i'th element is the estimated Beta1 for the i'th variable.
beta1_se = []  # The standard error of Beta1's. the i'th element is the standard error of Beta1 for the i'th variable.
sigma2_hats = []  # The sigma2's of the data. the i'th element is the estimated sigma for the i'th variable.
RSSs_train = []  # The Rss of the training data. the i'th element is the rss for the i'th variable if the training data.
RSSs_test = []  # The Rss of the testing data. the i'th element is the rss for the i'th variable of the test data.
R2_train = []  # The R^2 of the training data. the i'th element is the R^2 for the i'th variable of the test data.
R2_test = []  # The R^2 of the testing data. the i'th element is the R^2 for the i'th variable of the test data.
AIC_train = []  # The AIC of models for the training data.
AIC_test = []  # The AIC of models for the testing data.

for i in range(0, 8):
    beta1_hats.append(estimate_beta1(feature[i], Y))
    beta0_hats.append(estimate_beta0(feature[i], Y, beta1_hats[i]))
    # sigma2_hats.append(estimate_sigma2(feature[i], Y, beta0_hats[i], beta1_hats[i]))
    sigma2_hats.append(estimate_sigma2pr(Y, beta0_hats[i] + feature[i] * beta1_hats[i], Y.size, 1))
    beta0_se.append(calculate_se_beta0(sigma2_hats[i], feature[i]))
    beta1_se.append(calculate_se_beta1(sigma2_hats[i], feature[i]))
    # beta0_hats[i] + feature[i]*beta1_hats[i]) == vector of estimated y's
    RSSs_train.append(calculate_rss(Y, beta0_hats[i] + feature[i] * beta1_hats[i]))
    R2_train.append(calculate_r2(RSSs_train[i], Y))
    RSSs_test.append(calculate_rss(testing_data[:, 8], beta0_hats[i] + testing_data[:, i] * beta1_hats[i]))
    R2_test.append(calculate_r2(RSSs_test[i], Y))
    AIC_train.append(calculate_aic
                     (1,
                      calculate_log_likelihood(Y, beta0_hats[i] + testing_data[:, i] * beta1_hats[i],
                                               math.sqrt(sigma2_hats[i]))))
    AIC_test.append(calculate_aic
                    (1,
                     calculate_log_likelihood(testing_data[:, 8], beta0_hats[i] + testing_data[:, i] * beta1_hats[i],
                                              math.sqrt(sigma2_hats[i]))))

data_matrix = np.column_stack((['Models'].__add__(['Model ' + str(i) for i in range(1, 9)]),
                               ['Beta 0 Estimate'].__add__(beta0_hats),
                               ['Beta 1 Estimate'].__add__(beta1_hats),
                               ['Sigma 2 Estimate'].__add__(sigma2_hats),
                               ['Beta 1 standard error'].__add__(beta1_se),
                               ['Beta 0 standard error'].__add__(beta0_se),
                               ['RSS Test'].__add__(RSSs_test),
                               ['RSS Train'].__add__(RSSs_train),
                               ['R^2 Test'].__add__(R2_test),
                               ['R^2 Train'].__add__(R2_train),
                               ['AIC Test'].__add__(AIC_test),
                               ['AIC Train'].__add__(AIC_train)
                               ))

best_variable_index = RSSs_test.index(min(RSSs_test))  # Index of the best variable for later use.
# print(str(best_variable_index))

table = FF.create_table(data_matrix)
# plot(table, filename='Beta Table.html')  # Pyplot library draws the table in an html page.

# for i in range(0, 8):  # Plots Scatter plot and the estimated line.
#     plt.subplot(4, 2, i + 1)
#     plt.title('Variable ' + str(i + 1))
#     plt.xlabel('Variable ' + str(i + 1))
#     plt.ylabel('Target Variable')
#     plt.plot([float(feature_) for feature_ in feature[i]], beta0_hats[i] + beta1_hats[i] * feature[i], 'r')
#     plt.scatter(x=data[:, i], y=data[:, 8])

# plt.show()

# PART B END

# PART C and D START

# In this Part we use the data in matrix form.

used_parameters_index = [
    best_variable_index]  # An array that stores the index of the parameters that are currently used in the model
best_rsss = [RSSs_train[best_variable_index]]
best_R2s = [RSSs_train[best_variable_index]]
best_AICs = [AIC_train[best_variable_index]]

X_matrix_array = []  # The X matrix which contains the features we want to use in our model
Beta_hat_model = []  # The B matrix which has all the parameters for all the models each row is for one of the models.

Y_hats = []  # matrix that contains estimated Y's for all models.
RSSs_frw_test = []
RSSs_frw_train = []
R2s_frw_test = []
R2s_frw_train = []

model_gets_better = True

best_aic = AIC_train[best_variable_index]  # keep the current best aic to determine which feature to add.
best_r2 = R2_train[best_variable_index]
best_rss = RSSs_train[best_variable_index]
used_parameters_names = ['Features ' + used_parameters_index.__str__()]

while model_gets_better:

    AICs = []
    model_gets_better = False
    parameter_to_add_index = -1
    # A helper array which contains the names of all the parameters we used.

    for i in range(0, 8):
        if not used_parameters_index.__contains__(i):  # prevents adding duplicate features

            X_matrix_array = np.column_stack([1 for j in range(0, feature[0].size)])
            X_matrix_array.shape = (feature[0].size, 1)  # make a column of 1's for calculating beta0
            X_matrix_array = np.column_stack((X_matrix_array,
                                              feature[i]))

            for parameter_num in used_parameters_index:
                X_matrix_array = np.column_stack(
                    (X_matrix_array, feature[parameter_num]))  # add the parameters column's
                #  that we are using to X matrix.

            X_matrix = np.asmatrix(X_matrix_array)
            Y_matrix = np.asmatrix(np.column_stack(Y)).transpose()  # to make it a column matrix

            Beta_hat = np.dot \
                (np.linalg.inv
                 (np.dot(np.transpose(X_matrix), X_matrix)),
                 np.dot(np.transpose(X_matrix), Y_matrix))  # use Beta hat formula to compute beta_hat
            Beta_hat = np.transpose(Beta_hat)
            beta_hat_list = Beta_hat.tolist()

            string = ''
            for s in used_parameters_index:
                string = string + ' ' + str(s)

            Beta_hat_model.append(beta_hat_list[0])

            B_matrix = np.asmatrix(Beta_hat_model)  # Beta matrix that contains the parameters.
            Y_hat = np.dot(X_matrix, np.transpose(Beta_hat))  # Vector that contains the estimated Y's for each model.
            Y_hats.append(Y_hat)

            rss = calculate_rss(Y_matrix, Y_hat)
            RSSs_frw_test.append(rss)
            r2 = calculate_r2(rss, Y_matrix)
            R2s_frw_test.append(r2)
            sigma2 = estimate_sigma2pr(Y, Y_hat, Y.size, used_parameters_index.__len__())
            aic = calculate_aic(1, calculate_log_likelihood(Y, Y_hat, math.sqrt(sigma2)))

            if aic > best_aic:
                model_gets_better = True
                best_aic = aic
                parameter_to_add_index = i  # variable that remembers which of the features we should add the make the model better.
                best_r2 = r2
                best_rss = rss

    if model_gets_better:  # Checks to see if the model got better by adding the latest feature to the model
        used_parameters_index.append(parameter_to_add_index)
        used_parameters_names.append('Features ' + used_parameters_index.__str__())
        best_rsss.append(best_rss)  # Array that keeps all the RSS's of the different models.
        best_R2s.append(best_r2)  # Array that keeps all the R2's of the different models.
        best_AICs.append(best_aic)  # Array that keeps all the AIC's of the different models.

        # print used_parameters_index
        # print used_parameters_names
        # print best_rsss
        # print best_R2s
        # print best_AICs
        # print '--------------'

data_matrix_2 = np.column_stack((['Features'].__add__(used_parameters_names),
                                 ['RSS'].__add__(best_rsss),
                                 ['R2'].__add__(best_R2s),
                                 ['AIC'].__add__(best_AICs)))

table2 = FF.create_table(data_matrix_2)
# plot(table2, filename='Beta Table forwarding.html')

# PART C and D END

# PART E START

# print
all_features_matrix = np.column_stack([1 for i in range(0, feature[0].size)])
all_features_matrix.shape = (feature[0].size, 1)  # make a column of 1's for calculating beta0

for j in range(0, len(training_data[0]) - 1):
    all_features_matrix = np.column_stack(
        (all_features_matrix, training_data[:, j]))  # add the parameters column's
    #  that we are using to X matrix.

all_features_matrix = np.asmatrix(all_features_matrix)

Y_matrix = np.asmatrix(np.column_stack(Y)).transpose()  # to make it a column matrix
n = Y_matrix.size

#  In the below loop we calculate the Rcv n times each time omitting one of the rows of the matrix's and
#  calculating Y_hat then we calculate Rcv according to the equation in the book. and summing all of them

Y_hat_omitted = []
Rcv_error_n_times = 0
Rcv_error_one_time = 0

U = np.dot(np.dot(all_features_matrix, np.linalg.inv(np.dot(np.transpose(all_features_matrix), all_features_matrix))),
           np.transpose(all_features_matrix))

for i in range(0, n):
    omitted_feature_matrix = np.delete(all_features_matrix, i, axis=0)
    omitted_Y_matrix = np.delete(Y_matrix, i, axis=0)

    Beta_hat = np.dot \
        (np.linalg.inv
         (np.dot(np.transpose(omitted_feature_matrix), omitted_feature_matrix)),
         np.dot(np.transpose(omitted_feature_matrix), omitted_Y_matrix))  # use Beta hat formula to compute beta_hat
    Beta_hat = np.transpose(Beta_hat)

    Y_hat = np.dot(all_features_matrix,
                   np.transpose(Beta_hat))  # calculate Y_hat given that one of the features is omitted.
    # np.asarray(Y_hat[i]).tolist()[0] is equivalent to Y(i)_hat the estimation of Y(i) in which we omitted Yi and Xi's
    Rcv_error_n_times += math.pow(Y[i] - np.asarray(Y_hat[i]).tolist()[0], 2)

    Beta_hat = np.dot \
        (np.linalg.inv
         (np.dot(np.transpose(all_features_matrix), all_features_matrix)),
         np.dot(np.transpose(all_features_matrix), Y_matrix))  # use Beta hat formula to compute beta_hat
    Beta_hat = np.transpose(Beta_hat)

    Y_hat = np.dot(all_features_matrix, np.transpose(Beta_hat))  # calculate Y_hat using all of the features.

    Rcv_error_one_time += math.pow((Y[i] - Y_hat[i])
                                   / (1 - np.asarray(U.diagonal())[0][i]), 2)

data_matrix_3 = np.column_stack((['Rcv n times'].__add__([Rcv_error_n_times.__str__()]),
                                 ['Rcv n times'].__add__([Rcv_error_one_time.__str__()])))

table3 = FF.create_table(data_matrix_3)
# plot(table3, filename='Rcv errors.html')

# PART E END

# PART F START

all_features_matrix = np.delete(all_features_matrix, 0, axis=1)  # remove the columns of 1's
all_features_index = [i for i in range(0, X_matrix_array[0].__len__() - 1)]
removed_features_index = []
model_Rcvs = []
not_removed_feature_names = []
best_feature_to_remove = -1
best_rcv = -1
best_rss_backward = 0
rsss_backward = []

# print all_features_index

while removed_features_index.__len__() < 8:

    for j in range(0, all_features_index.__len__()):

        if not removed_features_index.__contains__(j):

            omitted_feature_matrix = all_features_matrix
            temp_remove = removed_features_index[:]
            temp_remove.append(j)

            # for remove_feature in removed_features_index:
            omitted_feature_matrix = np.delete(omitted_feature_matrix, temp_remove,
                                               axis=1)  # remove the columns of the features we don't want in the models.

            # print omitted_feature_matrix
            omitted_feature_matrix = np.column_stack(([1 for i in range(0, feature[0].size)], omitted_feature_matrix))
            # print omitted_feature_matrix

            Rcv_error_one_time = 0

            U = np.dot(np.dot(omitted_feature_matrix,
                              np.linalg.inv(np.dot(np.transpose(omitted_feature_matrix), omitted_feature_matrix))),
                       np.transpose(omitted_feature_matrix))

            for i in range(0, n):
                Beta_hat = np.dot \
                    (np.linalg.inv
                     (np.dot(np.transpose(omitted_feature_matrix), omitted_feature_matrix)),
                     np.dot(np.transpose(omitted_feature_matrix), Y_matrix))  # use Beta hat formula to compute beta_hat
                Beta_hat = np.transpose(Beta_hat)

                Y_hat = np.dot(omitted_feature_matrix,
                               np.transpose(Beta_hat))  # calculate Y_hat using all of the features.

                Rcv_error_one_time += math.pow((Y[i] - Y_hat[i])
                                               / (1 - np.asarray(U.diagonal())[0][i]), 2)

            if best_rcv == -1:
                best_rcv = Rcv_error_one_time
                best_feature_to_remove = j
                best_rss_backward = calculate_rss(Y, Y_hat)

            if best_rcv > Rcv_error_one_time:
                best_rcv = Rcv_error_one_time
                best_feature_to_remove = j
                best_rss_backward = calculate_rss(Y, Y_hat)

    rsss_backward.append(best_rss_backward)
    removed_features_index.append(best_feature_to_remove)
    model_Rcvs.append(best_rcv)
    not_removed_feature_names.append(
        'Features ' + [x for x in all_features_index if x not in removed_features_index].__str__())

    best_rcv = -1

data_matrix_4 = np.column_stack((['Features'].__add__(not_removed_feature_names),
                                 ['RSS'].__add__(rsss_backward),
                                 ['Rcv'].__add__(model_Rcvs)))

table4 = FF.create_table(data_matrix_4)
# plot(table4, filename='Backward methods table.html')


# for i in range(0, 8):  # Plots Scatter plot and the estimated line.
#     plt.subplot(4, 2, i + 1)

# print (model_Rcvs)
# print range(1, 9)

# plt.title('Rcv Plot')
# plt.xlabel('Feature numbers')
# plt.ylabel('Rcv')
# plt.plot(range(1, 9), model_Rcvs)

# plt.show()

# plt.title('RSS plot')
# plt.xlabel('Feature Numbers')
# plt.ylabel('RSS')
# plt.plot(range(1, 9), rsss_backward)

# plt.show()

# PART F END


# PART G START

best_model_features = [0, 2, 3, 4, 6, 7]
best_model_removed_features = [1, 5]
best_model_x_matrix = all_features_matrix[:]
best_model_x_matrix = np.delete(best_model_x_matrix, best_model_removed_features, axis=1)
best_model_x_matrix = np.column_stack(
    ([1 for i in range(0, feature[0].size)], best_model_x_matrix))  # add a column of 1's to the matrix

best_model_test_x_matrix = np.delete(X_test_matrix_array, best_model_removed_features, axis=1)  # we must remove the unwanted features from the test data too

print '---------------'

one_hundred_data = np.asarray(best_model_x_matrix[:100])
one_hundred_data_Y = Y_matrix[:100]
two_hundred_data = np.asarray(best_model_x_matrix[:200])
two_hundred_data_Y = Y_matrix[:200]
three_hundred_data = np.asarray(best_model_x_matrix[:300])
three_hundred_data_Y = Y_matrix[:300]
all_data = best_model_x_matrix
all_Y = Y_matrix

array_X = [one_hundred_data, two_hundred_data, three_hundred_data, all_data]
array_Y = [one_hundred_data_Y, two_hundred_data_Y, three_hundred_data_Y, all_Y]

RSSs_train = []
RSSs_test = []

for X, Y in itertools.izip(array_X, array_Y):
    Beta_hat = np.dot \
        (np.linalg.inv
         (np.dot(np.transpose(X), X)),
         np.dot(np.transpose(X), Y))  # use Beta hat formula to compute beta_hat
    Beta_hat = np.transpose(Beta_hat)

    Y_hat_train = np.dot(X, np.transpose(Beta_hat))

    rss_current_train = calculate_rss(Y, Y_hat_train)
    RSSs_train.append(rss_current_train)

    Y_hat_test = np.dot(best_model_test_x_matrix, np.transpose(Beta_hat))
    rss_current_test = calculate_rss(Y_test, Y_hat_test)
    RSSs_test.append(rss_current_test)

# plt.title('RSS plot Train')
# plt.xlabel('Data Count * 100')
# plt.ylabel('RSS')
# plt.plot(range(1, array_X.__len__() + 1), RSSs_train)
#
# plt.show()
#
# plt.title('RSS plot Test')
# plt.xlabel('Data Count * 100')
# plt.ylabel('RSS')
# plt.plot(range(1, array_X.__len__() + 1), RSSs_test)
#
# plt.show()

# PART G END
