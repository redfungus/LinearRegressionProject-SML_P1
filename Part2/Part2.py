import csv
import itertools
import math
import numpy as np
import matplotlib.pyplot as plt
import plotly.plotly as py
from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
from plotly.tools import FigureFactory as FF
from scipy import stats
from sklearn import linear_model
from snowballstemmer.danish_stemmer import lab0


def estimate_beta1(x, y):  # Function that estimates Beta1
    x_y_covariance = np.cov(x, y)  # by devising X and Y's Covariance by X's variance which is equal to equation (14.5)
    x_variance = np.var(x)
    return x_y_covariance[0][1] / x_variance


def calculate_log_likelihood(y, y_hat, sigma):
    n = y.size
    return -n * math.log10(sigma) - ((1 / 2 * math.pow(sigma, 2)) * calculate_rss(y, y_hat))


def calculate_aic(parameter_num, log_likelihood):
    return log_likelihood - parameter_num


def calculate_rss(y, y_hat):
    rss = 0  # estimates sigma square by using the estimates of Y
    for y_hati, yi in itertools.izip(y_hat, y):
        epsilon = yi - y_hati
        rss += math.pow(epsilon, 2)
    return rss


def estimate_beta0(x, y, beta1hat):
    return np.mean(y) - beta1hat * np.mean(x)


def estimate_sigma2(x, y, beta0hat, beta1hat):  # Function that takes in all the X's and the Y's and
    rss = 0  # estimates sigma square by using the estimates of Y
    for xi, yi in itertools.izip(x, y):
        epsilon = yi - (beta0hat + beta1hat * xi)
        rss += math.pow(epsilon, 2)
    return rss / (x.size - 2)


def estimate_sigma2pr(y, y_hat, n, k):  # Function that takes in all the X's and the Y's and
    rss_ = 0  # estimates sigma square by using the estimates of Y
    for yi, y_hati in itertools.izip(y,
                                     y_hat):  # Another method like the one above but tuned to be convient for forwarding method.
        epsilon = yi - y_hati
        rss_ += math.pow(epsilon, 2)
    return rss_ / (n - k)


def calculate_r2(rss, y):
    y_bar = np.mean(y)
    tss = 0
    for yi in y:
        tss += math.pow(yi - y_bar, 2)
    return 1 - (rss / tss)


def calculate_se_beta0(sigma_hat, x):
    sx2 = np.var(x)
    x2 = pow(x, 2)
    return (sigma_hat / (math.sqrt(sx2) * math.sqrt(x.size))) * (math.sqrt(sum(x2) / x.size))


def calculate_se_beta1(sigma_hat, x):
    sx2 = np.var(x)
    return sigma_hat / (math.sqrt(sx2) * math.sqrt(x.size))


# PREPARATION START

with open('Dataset2.csv', 'rb') as csv_file:
    spam_reader = csv.reader(csv_file, delimiter=' ')
    data_ = []  # Array which we load the data file into
    for row in spam_reader:
        data_.append(str(row[0]).split(','))

data_ = [[float(string) for string in inner] for inner in data_]  # Convert the strings to int's for easy usage.
data = np.array(data_)  # convert the data array to a numpy array to be able to use it's features.

with open('Dataset2_Unlabeled.csv', 'rb') as csv_file:
    spam_reader = csv.reader(csv_file, delimiter=' ')
    data_unlabeled_ = []  # Array which we load the data file into
    for row in spam_reader:
        data_unlabeled_.append(str(row[0]).split(','))

data_unlabeled_ = [[float(string) for string in inner] for inner in
                   data_unlabeled_]  # Convert the strings to int's for easy usage.
data_unlabeled = np.array(data_unlabeled_)  # convert the data array to a numpy array to be able to use it's features.

# print(data_)
# data[x][y] x determines which data we want, y determines which feature we want.
# the 9th element in each row is the target variable.
# e.g: data[1][2] is the third FEATURE in the second row of DATA.
# e.g data[:, 1] is the array of all the second FEATURES (second column of all the data)
training_data = data[:200]
testing_data = data[200:]
training_data_unlabeled = data_unlabeled

feature = []  # The i'th feature is feature[i]
feature_test = []
for i in range(0, 6):
    feature.append(training_data[:, i])
    feature_test.append(testing_data[:, i])
Y = training_data[:, 6]  # List of the target variables.
Y_test = testing_data[:, 6]
Yn_bar = np.mean(Y)  # Mean of target variables.

feature_unlabeled = []  # The i'th feature is feature[i]
for i in range(0, 6):
    feature_unlabeled.append(training_data_unlabeled[:, i])
# Y_unlabeled = training_data_unlabeled[:, 6]  # List of the target variables.
# Yn_bar_unlabeled = np.mean(Y_unlabeled)  # Mean of target variables.

X_matrix_array = np.column_stack([1 for j in range(0, feature[0].size)])
X_matrix_array.shape = (feature[0].size, 1)  # make a column of 1's for calculating beta0
for i in range(0, 6):
    X_test_matrix_array = np.column_stack((X_matrix_array,
                                           feature[i]))

# PREPARATION END

# PART A START

# for i in range(0, 6):  # Scatter plot start.
#     plt.subplot(3, 2, i + 1)
#     plt.title('Variable ' + str(i+1))
#     plt.xlabel('Variable ' + str(i+1))
#     plt.ylabel('Target Variable')
#     plt.scatter(x=data[:, i], y=data[:, 6])

# Uncomment this line to plot the figures.
# plt.show()  # Scatter plot end.

# PART A END

# PART B START

filled_feature = []

for current_feature in feature:
    median = np.median(current_feature[current_feature > 0])  # computes median of non zero elements.
    current_feature[current_feature == 0] = median
    filled_feature.append(current_feature)  # replace 0's with the median and make a new list.

filled_feature_test = []

for current_feature in feature_test:
    median = np.median(current_feature[current_feature > 0])  # computes median of non zero elements.
    current_feature[current_feature == 0] = median
    filled_feature_test.append(current_feature)  # replace 0's with the median and make a new list.

filled_feature_unlabeled = []

for current_feature in feature_unlabeled:
    median = np.median(current_feature[current_feature > 0])  # computes median of non zero elements.
    current_feature[current_feature == 0] = median
    filled_feature_unlabeled.append(current_feature)  # replace 0's with the median and make a new list.

# for i in range(0, 6):  # Scatter plot start.
#     plt.subplot(3, 2, i + 1)
#     plt.title('Variable ' + str(i+1))
#     plt.xlabel('Variable ' + str(i+1))
#     plt.ylabel('Target Variable')
#     plt.scatter(x=feature[i], y=data[:, 6])
#
# plt.show()

# PART B END

# PART C START

# the method gets the X matrix in shape (sample_count, feature count) so we transpose the X matrix

clf = linear_model.Lasso(alpha=2)
clf.fit(np.asarray(filled_feature).transpose(), Y)

data_matrix = []

for idx, coef in enumerate(clf.coef_):
    data_matrix.append(['Feature ' + idx.__str__()].__add__([coef]))
    table = FF.create_table(np.asarray(data_matrix).transpose())

# plot(table, filename='Estimated Coefficients.html')  # Pyplot library draws the table in an html page.

# PART C END

# PART D START

rss_tests = []
rss_trains = []
r2_tests = []
r2_trains = []
lambdas = []

for alpha_ in np.arange(1, 4, 0.1):
    clf = linear_model.Lasso(alpha=alpha_)
    clf.fit(np.asarray(filled_feature).transpose(), Y)
    Beta_hat = clf.coef_
    Y_hat = np.dot(np.asarray(filled_feature).transpose(), np.asarray(Beta_hat).transpose())
    Y_hat_test = np.dot(np.asarray(filled_feature_test).transpose(), np.asarray(Beta_hat).transpose())
    rss = calculate_rss(Y, Y_hat)
    r2 = calculate_r2(rss, Y)
    r2_trains.append(r2)
    rss_trains.append(rss)

    rss = calculate_rss(Y_test, Y_hat_test)
    r2 = calculate_r2(rss, Y_test)
    r2_tests.append(r2)
    rss_tests.append(rss)

    best_model = clf
    lambdas.append(alpha_)

print (r2_trains)
print (rss_trains)

print (r2_tests)
print (rss_tests)

data_matrix2 = np.column_stack((['Lambda'].__add__(lambdas),
                                ['RSS Test'].__add__(rss_tests),
                                ['RSS Train'].__add__(rss_trains),
                                ['R^2 Test'].__add__(r2_tests),
                                ['R^2 Train'].__add__(r2_trains)
                                ))

table2 = FF.create_table(data_matrix2)
# plot(table2, filename='Lambdas.html')  # Pyplot library draws the table in an html page.

Y_hat_unlabeled = best_model.predict(np.asarray(feature_unlabeled).transpose())
print (Y_hat_unlabeled)

# plt.title('RSS plot Train')
# plt.xlabel('Data Count * 100')
# plt.ylabel('RSS')
# plt.plot(np.arange(1, 4, 0.1), rss_trains)
#
# plt.show()
#
# plt.title('R2 plot Train')
# plt.xlabel('Data Count * 100')
# plt.ylabel('R2')
# plt.plot(np.arange(1, 4, 0.1), r2_trains)
#
# plt.show()
#
# plt.title('RSS plot Test')
# plt.xlabel('Data Count * 100')
# plt.ylabel('RSS')
# plt.plot(np.arange(1, 4, 0.1), rss_tests)
#
# plt.show()
#
# plt.title('R2 plot Test')
# plt.xlabel('Data Count * 100')
# plt.ylabel('RSS')
# plt.plot(np.arange(1, 4, 0.1), r2_tests)
#
# plt.show()

# PART D END
